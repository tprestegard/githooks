# githooks
This repository provides a framework for applying a "global" set of `git` hooks which can be used across multiple repositories.
It is possible to either ignore the global hooks, or use a set of local, repository-specific hooks in conjunction with the global hooks.


## Configuration

### Generate symlinks
This framework defines all possible `git` hooks as symlinks to a helper script which dynamically loads any global or local hooks.
To generate the required symlinks, run the `generate-symlinks.sh` script from the root of this repository:
```
./bin/generate-symlinks.sh
```

This script will also create the `hooks` directory, which holds the symlinks.

### Set up global hooks
Set your global `core.hookspath` config variable to be the `hooks` directory in this repository.
In this example, we have symlinked the root of this repository to be at `~/.githooks`.
```
git config --global core.hookspath ~/.githooks/hooks
```


### Individual repository configuration
Individual repositories may be configured on a case-by-case basis.

#### Using only local hooks
Just override the global value for `core.hookspath` with a local configuration, providing the path to a repository-specific directory of hooks:
```
git config --local core.hookspath .hooks
```

#### Using only global hooks
Do nothing - the global configuration takes care of this.

#### Using both global and local hooks
* Leave `core.hookspath` as the global setting.
* Set `core.localhookspath` to your local hooks directory. Example:
  ```
  git config --local core.localhookspath .hooks
  ```
* Set the merge strategy for combining global and local hooks:
  ```
  git config --local core.hookmergestrategy <val>
  ```
  where `<val>` can be 0 or 1:
  * 0: if a corresponding local hook exists, skip the global hook. Otherwise, run the global hook.
  * 1: run global hooks, then local hooks, if they exist.
* If `core.hookmergestrategy` is not set, then it will default to 0.

## How it works
The entrypoint into a `git` hook is the symlink in the `hooks` directory.
This link points to `helpers/_hook_init`, which calls `helpers/_hook_runner` with the name of the hook and any other arguments passed to the hook by `git`.
The runner script checks whether a local hook exists and whether a global hook exists.
If a global hook exists and should be run (depending on the merge strategy), it is triggered.
Then, if a local hook exists and should be run, it is also triggered.

## Creating hooks
First, make sure a symlink exists for the hook in `hooks` and is linked to `helpers/_hook_init`.
I believe I have covered all existing `git` hooks, but it is possible that more will be added in the future.
This can be done by updating and re-running `bin/generate-symlinks.sh`.

### Local hooks
Create the hook however you would for a normal `git` hook!

### Global hooks
Create a `bash` script in `hooks.d` corresponding to the name of the hook.
This script should contain:
* One or more "helper functions", each one corresponding to an individual process which should be triggered for this hook. These functions should call `exit 1` upon failure.
* A special `_run` function, which calls all of the other helper functions.

The script will be passed any arguments which would normally be passed to a `git` hook, and these arguments may be passed on to the check functions.

See `hooks.d/commit-msg` for a working example with multiple checks.

## Skipping hooks
Set `NO_VERIFY=true` before running a `git` operation to skip global hooks.
Example:
```
NO_VERIFY=true git rebase -i HEAD~5
```
