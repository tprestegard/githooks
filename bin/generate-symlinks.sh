#! /bin/bash

# List of all possible hooks
HOOKS=(
    applypatch-msg
    commit-msg
    fsmonitor-watchman
    pre-rebase
    p4-pre-submit
    pre-applypatch
    pre-auto-gc
    pre-commit
    pre-merge-commit
    prepare-commit-msg
    post-applypatch
    post-checkout
    post-commit
    post-index-change
    post-merge
    post-receive
    post-rewrite
    pre-push
    pre-receive
    push-to-checkout
    sendemail-validate
    update
)

# Get root path
ROOT_PATH=$(realpath "$(dirname $0)/..")

# Create hooks directory if it doesn't exist
if [[ ! -d "${ROOT_PATH}/hooks" ]]; then
    mkdir "${ROOT_PATH}/hooks"
fi

# Create symlinks if they don't exist
for HOOK in "${HOOKS[@]}"; do
    if [[ ! -e "${ROOT_PATH}/hooks/${HOOK}" ]]; then
        ln -sr "${ROOT_PATH}/helpers/_hook_init" "${ROOT_PATH}/hooks/${HOOK}"
    fi
done
